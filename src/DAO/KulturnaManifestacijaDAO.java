package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Grad;
import model.KulturnaManifestacija;


public class KulturnaManifestacijaDAO {

	public static KulturnaManifestacija getManifestacijaById(Connection conn, int id) {
		KulturnaManifestacija km = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT naziv, brojPosetilaca FROM manifestacija WHERE id_manifestacija = " + id);

			if (rset.next()) {
				String naziv = rset.getString(1);
				int broj = rset.getInt(2);
				
				km = new KulturnaManifestacija(id, naziv,broj);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return km;
	}

	public static KulturnaManifestacija getManifestacijaByNaziv(Connection conn, String naziv) {
		KulturnaManifestacija km = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT id_manifestacija, brojPosetilaca " +
							"FROM manifestacija WHERE naziv = '" 
							+ naziv + "'");

			if (rset.next()) {
				int id = rset.getInt(1);
				int broj = rset.getInt(2);
				
				km = new KulturnaManifestacija(id, naziv, broj);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return km;
	}
	
	
	public static List<KulturnaManifestacija> getAllManifestacija(Connection conn) {
		List<KulturnaManifestacija> retVal = new ArrayList<KulturnaManifestacija>();
		
		try {
			String query = "SELECT id_manifestacija, naziv, brojPosetilaca FROM manifestacija ";
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query.toString());
			while (rset.next()) {
				int id = rset.getInt(1);
				String naziv = rset.getString(2);
				int broj = rset.getInt(3);
				
				String select = "select id_grad from odrzava"
						+ " where id_manifestacija = " + id;
				Statement stmt1 = conn.createStatement();
				ResultSet rset1 = stmt1.executeQuery(select.toString());
				Grad gr = null;
				while (rset1.next()) {
					int idGrad = rset1.getInt(1);
					gr = GradDAO.getGradById(conn, idGrad);
				}
				rset1.close();
				stmt1.close();
				KulturnaManifestacija km = new KulturnaManifestacija(id, naziv,broj);
				km.setGrad(gr);
				retVal.add(km);
				
				}
			rset.close();
			stmt.close();
			return retVal;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean add(Connection conn, KulturnaManifestacija km){
		boolean retVal = false;
		try {
			String update = "INSERT INTO manifestacija ( naziv, " +
					"brojPosetilaca) values (?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, km.getNaziv());
			pstmt.setInt(2, km.getBrojPosetilaca());
			if(pstmt.executeUpdate() == 1){
				retVal = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean update(Connection conn, KulturnaManifestacija km) {
		boolean retVal = false;
		try {
			String update = "UPDATE manifestacija SET naziv=?," +
					" brojPosetilaca=? WHERE id_manifestacija=?";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, km.getNaziv());
			pstmt.setInt(2, km.getBrojPosetilaca());
			pstmt.setInt(3, km.getId());
			
			if(pstmt.executeUpdate() == 1)
				retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean delete(Connection conn, int id) {
		boolean retVal = false;
		try {
			String update = "DELETE FROM manifestacija WHERE " +
					"id_manifestacija = " + id;
			Statement stmt = conn.createStatement();
			if (stmt.executeUpdate(update) == 1)
				retVal = true;
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	
}
