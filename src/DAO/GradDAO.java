package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Grad;
import model.KulturnaManifestacija;

public class GradDAO {

	public static Grad getGradById(Connection conn, int id) {
		Grad Grad = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT ptt, naziv " +
							"FROM grad WHERE id_grad = " 
							+ id);

			if (rset.next()) {
				int ptt = rset.getInt(1);
				String naziv = rset.getString(2);
				
				
				Grad = new Grad(id, ptt, naziv);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Grad;
	}
	
	public static Grad getGradByNaziv(Connection conn, String naziv) {
		Grad grad = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT id_grad, ptt " +
							"FROM grad WHERE naziv = '" 
							+ naziv + "'");

			if (rset.next()) {
				int id = rset.getInt(1);
				int ptt = rset.getInt(2);
				
				grad = new Grad(id, ptt, naziv);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return grad;
	}
	
	public static List<Grad> getAll(Connection conn) {
		List<Grad> retVal = new ArrayList<Grad>();
		try {
			String query = "SELECT id_grad, ptt, naziv FROM grad ";
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query.toString());
			while (rset.next()) {
				int id = rset.getInt(1);
				int ptt = rset.getInt(2);
				String naziv = rset.getString(3);
				
				String upitZaManifestacije = "select id_manifestacija from " +
						"odrzava where id_grad = " + id;
				Statement stmtZaManifestaciju = conn.createStatement();
				ResultSet rsetManifestacija = stmtZaManifestaciju.executeQuery(
						upitZaManifestacije);
				List<KulturnaManifestacija> man = new ArrayList<KulturnaManifestacija>();
				while (rsetManifestacija.next()) {
					int idManifestacija = rsetManifestacija.getInt(1);
					KulturnaManifestacija km = KulturnaManifestacijaDAO.getManifestacijaById(conn, idManifestacija);
					man.add(km);
				}
				stmtZaManifestaciju.close();
				rsetManifestacija.close();
				
				Grad grad = new Grad(id, ptt, naziv);
				grad.setManifestacije(man);
				retVal.add(grad);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean update(Connection conn, Grad grad) {
		boolean retVal = false;
		try {
			String update = "UPDATE grad SET ptt=?," +
					" naziv=? WHERE id_grad=?";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setInt(1, grad.getPtt());
			pstmt.setString(2, grad.getNaziv());
			pstmt.setInt(3, grad.getId());
			
			if(pstmt.executeUpdate() == 1)
				retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean delete(Connection conn, int id) {
		boolean retVal = false;
		try {
			String update = "DELETE FROM grad WHERE " +
					"id_grad = " + id;
			Statement stmt = conn.createStatement();
			if (stmt.executeUpdate(update) == 1)
				retVal = true;
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}

}
