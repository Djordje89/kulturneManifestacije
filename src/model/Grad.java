package model;

import java.util.ArrayList;
import java.util.List;

public class Grad {

	int id;
	int ptt;
	String naziv;
	List <KulturnaManifestacija> manifestacije = new ArrayList<KulturnaManifestacija>();
	
	
	public Grad(int id, int ptt, String naziv) {
		super();
		this.id = id;
		this.ptt = ptt;
		this.naziv = naziv;
	}

	

	public Grad(int id, int ptt, String naziv, List<KulturnaManifestacija> manifestacije) {
		super();
		this.id = id;
		this.ptt = ptt;
		this.naziv = naziv;
		this.manifestacije = manifestacije;
	}



	@Override
	public String toString() {
		return "Grad [" + id + ", " + ptt + ", " + naziv + "]";
	}

	public String toFileRepresentationAll(){
		StringBuilder sb = new StringBuilder("Grad [" + id + ", " + ptt + ", " + naziv + "]");
		if(manifestacije != null){
			sb.append(" ima sledece kulturne manifestacije:\n");
			for (KulturnaManifestacija kulturnaManifestacija : manifestacije) {
				sb.append("\t" + kulturnaManifestacija.toString() + "\n");
			}
		}
		return sb.toString();
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grad other = (Grad) obj;
		if (id != other.id)
			return false;
		return true;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getPtt() {
		return ptt;
	}


	public void setPtt(int ptt) {
		this.ptt = ptt;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}



	public List<KulturnaManifestacija> getManifestacije() {
		return manifestacije;
	}



	public void setManifestacije(List<KulturnaManifestacija> manifestacije) {
		this.manifestacije = manifestacije;
	}
	
	
}
