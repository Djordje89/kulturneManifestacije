package model;

public class KulturnaManifestacija {

	int id;
	String naziv;
	int brojPosetilaca;
	Grad grad;
	
	public KulturnaManifestacija(int id, String naziv, int brojPosetilaca) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.brojPosetilaca = brojPosetilaca;
	}

	public KulturnaManifestacija(int id, String naziv, int brojPosetilaca, Grad grad) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.brojPosetilaca = brojPosetilaca;
		this.grad = grad;
	}

	@Override
	public String toString() {
		return "KulturnaManifestacija [" + id + ", " + naziv + ", " + brojPosetilaca + "]";
	}
	
	public String toFileRepresentationAll(){
		StringBuilder sb = new StringBuilder("KulturnaManifestacija [" + id + ", " + naziv + ", " + brojPosetilaca + "]");
		if(grad != null)
			sb.append("\ni odrzava se u: " + grad);
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KulturnaManifestacija other = (KulturnaManifestacija) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getBrojPosetilaca() {
		return brojPosetilaca;
	}

	public void setBrojPosetilaca(int brojPosetilaca) {
		this.brojPosetilaca = brojPosetilaca;
	}

	public Grad getGrad() {
		return grad;
	}

	public void setGrad(Grad grad) {
		this.grad = grad;
	}
	
	
	
	
	
}
