package ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import DAO.KulturnaManifestacijaDAO;
import model.KulturnaManifestacija;
import util.PomocnaKlasa;

public class KulturnaManifestacijaUI {

	public static void menu() {
		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				ispisiSveManifestacije();
				break;
			case 2:
				KulturnaManifestacija km = pronadjiManifestaciju();
				System.out.println(km);
				break;
			case 3:
				unosNoveManifestacije();
				break;
			case 4:
				izmenaPodatakaOManifestaciji();
				break;
			case 5:
				brisanjePodatakaOManifestaciji();
				break;
			case 6:
				izvestajManifestacijeSaNajviseLjudi();
				break;
			case 7:
				try {
					citajIzFajlaIzvestaj();
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void ispisiMenu() {
		System.out.println("Rad sa studentima - opcije:");
		System.out.println("\tOpcija broj 1 - ispis svih Manifestacija");
		System.out.println("\tOpcija broj 2 - pronadji Kulturnu Manifestaciju");
		System.out.println("\tOpcija broj 3 - unos nove manifestacije");
		System.out.println("\tOpcija broj 4 - izmena Manifestacije");
		System.out.println("\tOpcija broj 5 - brisanje Manifestacije");
		System.out.println("\tOpcija broj 6 - isvestaj najpopularnije kulturne manifestacije");
		System.out.println("\tOpcija broj 7 - citanje iz trenutnog fajla izvestaj");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}

	
	public static void ispisiSveManifestacije() {
		List<KulturnaManifestacija> sveManifestacije = KulturnaManifestacijaDAO.getAllManifestacija(AplicationUI.conn);
		for (int i = 0; i < sveManifestacije.size(); i++) {
			System.out.println(sveManifestacije.get(i));
		}
	}

	public static KulturnaManifestacija pronadjiManifestaciju() {
		KulturnaManifestacija retVal = null;
		System.out.print("Unesi id kulturne manifestacije:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		retVal = KulturnaManifestacijaDAO.getManifestacijaById(AplicationUI.conn, id);
		if (retVal == null)
			System.out.println("Manifestacija sa id-jem " + id
					+ " ne postoji u evidenciji");
		return retVal;
	}
	
	public static KulturnaManifestacija pronadjiManifestaciju(String naziv) {
		KulturnaManifestacija retVal = KulturnaManifestacijaDAO.getManifestacijaByNaziv(AplicationUI.conn,
				naziv);
		return retVal;
	}
	
	
	public static void unosNoveManifestacije() {
		
		System.out.print("Unesi naziv:");
		String naziv = PomocnaKlasa.ocitajTekst();
		while (pronadjiManifestaciju(naziv) != null) {
			System.out.println("Manifestacija sa nazivom " + naziv
					+ " vec postoji");
			naziv = PomocnaKlasa.ocitajTekst();
		}
		
		System.out.print("Unesi broj Posetilaca:");
		int broj = PomocnaKlasa.ocitajCeoBroj();

		KulturnaManifestacija km = new KulturnaManifestacija(0,  naziv, broj);
		// ovde se moze proveravati i povratna vrednost i onda ispisivati poruka
		KulturnaManifestacijaDAO.add(AplicationUI.conn, km);
	}

	// izmena studenta
	public static void izmenaPodatakaOManifestaciji() {
		KulturnaManifestacija km1 = pronadjiManifestaciju();
		if (km1 != null) {

			System.out.print("Unesi naziv :");
			String kmNaziv = PomocnaKlasa.ocitajTekst();
			km1.setNaziv(kmNaziv);

			System.out.print("Unesi broj posetilaca:");
			int kmBroj = PomocnaKlasa.ocitajCeoBroj();
			km1.setBrojPosetilaca(kmBroj);
	
			KulturnaManifestacijaDAO.update(AplicationUI.conn, km1);
		}
	}

	// brisanje studenta
	public static void brisanjePodatakaOManifestaciji() {
		KulturnaManifestacija km = pronadjiManifestaciju();
		if (km != null) {
			KulturnaManifestacijaDAO.delete(AplicationUI.conn, km.getId());
		}
	}
	
	public static void izvestajManifestacijeSaNajviseLjudi() {
		List<KulturnaManifestacija> sveManifestacije = KulturnaManifestacijaDAO.getAllManifestacija(AplicationUI.conn);
		KulturnaManifestacija km1 = null;
		int pom = 0;
		for (int i = 0; i < sveManifestacije.size(); i++) {
			if (sveManifestacije.get(i).getBrojPosetilaca() > pom) {
				pom = sveManifestacije.get(i).getBrojPosetilaca();
				km1 = sveManifestacije.get(i);
			}
		}
		System.out.println("Izvestaj za manifestaciju sa najvecom posecenoscu.");
		System.out.println(km1);

		try {

			String sP = System.getProperty("file.separator");

			File manifestacijaFajl = new File("." + sP + "data" + sP + "izvestaj.txt");

			PrintWriter out2 = new PrintWriter(new FileWriter(manifestacijaFajl));
			out2.println(km1.getNaziv() + " - " + km1.getBrojPosetilaca());

			out2.flush();
			out2.close();
		} catch (IOException e) {
			System.err.println("Greska pri upisu u fajl - Izvestaj");
		}
	}
	
	static void citajIzFajlaIzvestaj() throws IOException {
		
		List <KulturnaManifestacija> sveManifestacije = new ArrayList<KulturnaManifestacija>();
		String sP = System.getProperty("file.separator");
		
		File manifestacijaFajl = new File("."+sP+"data"+sP+"izvestaj.txt");
		if(manifestacijaFajl.exists()){
			
			try{
				
			BufferedReader in = new BufferedReader(new FileReader(manifestacijaFajl));

			in.mark(1); //zapamti trenutnu poziciju u fajlu da mozes kasnije da se vratis na nju
			if(in.read()!='\ufeff'){
				in.reset();
			}
			
			int brojac = 1;
			String s2;
			while((s2 = in.readLine()) != null) {
				String [] tokeni = s2.split(" - ");
				sveManifestacije.add(new KulturnaManifestacija(brojac,tokeni[0],Integer.parseInt(tokeni[1])));
				brojac++;
			}
			in.close();
			}catch(IOException e){
				System.err.println("Greska pri citanju iz fajla - izvestaj");
			}
			
		} else {
			System.out.println("Ne postoji fajl!");
		}
		
		for (KulturnaManifestacija kulturnaManifestacija : sveManifestacije) {
			System.out.println(kulturnaManifestacija.toString());
		}
	}
	

}
