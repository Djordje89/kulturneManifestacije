package ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import util.PomocnaKlasa;

public class AplicationUI {

	public static Connection conn;
	
	static {

		try {
			Class.forName("com.mysql.jdbc.Driver");

			// konekcija
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/vezbe8", "root", "jwd15_14");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public static void main(String [] args){
		
		int odluka = -1;
		while (odluka != 0) {
			AplicationUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				KulturnaManifestacijaUI.menu();
				break;
			case 2:
				GradUI.menu();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}
	
	// ispis teksta osnovnih opcija
	public static void ispisiMenu() {
		System.out.println("Kulturne manifestacije i njihovi gradovi - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Rad sa kulturnim manifestacijama");
		System.out.println("\tOpcija broj 2 - Rad sa gradovima");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

		
		
		
		
}

