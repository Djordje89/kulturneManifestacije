package ui;

import java.util.List;

import DAO.GradDAO;
import DAO.KulturnaManifestacijaDAO;
import model.Grad;
import model.KulturnaManifestacija;
import util.PomocnaKlasa;

public class GradUI {

	public static void menu() {
		int odluka = -1;
		while (odluka != 0) {
			GradUImeni();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				ispisiSveGradove();
				break;
			case 2:
				ispisiSveGradoveSaManifestacijama();
				break;
			case 3:
				izmenaPodatakaOGradu();
				break;
			case 4:
				brisanjePodatakaOGradu();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void GradUImeni() {
		System.out.println("Rad sa studentima - opcije:");
		System.out.println("\tOpcija broj 1 - ispis svih Gradova");
		System.out.println("\tOpcija broj 2 - ispis svih Gradova sa njihovim manifestacijama");
		System.out.println("\tOpcija broj 3 - izmena Grada");
		System.out.println("\tOpcija broj 4 - brisanje Grada");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}

	public static void ispisiSveGradove() {
		List<Grad> sviGradovi = GradDAO.getAll(AplicationUI.conn);
		for (int i = 0; i < sviGradovi.size(); i++) {
			System.out.println(sviGradovi.get(i));
		}
	}
	
	public static void ispisiSveGradoveSaManifestacijama() {
		List<Grad> sviGradovi = GradDAO.getAll(AplicationUI.conn);
		for (int i = 0; i < sviGradovi.size(); i++) {
			System.out.println(sviGradovi.get(i).toFileRepresentationAll());
		}
	}
	
	public static Grad pronadjiGrad() {
		Grad retVal = null;
		System.out.print("Unesi id Grada:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		retVal = GradDAO.getGradById(AplicationUI.conn, id);
		if (retVal == null)
			System.out.println("gRAD sa id-jem " + id
					+ " ne postoji u evidenciji");
		return retVal;
	}
	
	public static Grad pronadjiGrad(String naziv) {
		Grad retVal = GradDAO.getGradByNaziv(AplicationUI.conn,
				naziv);
		return retVal;
	}
	
	public static void izmenaPodatakaOGradu() {
		Grad grad = pronadjiGrad();
		if (grad != null) {

			System.out.print("Unesi ptt grada:");
			int pttGrad = PomocnaKlasa.ocitajCeoBroj();
			grad.setPtt(pttGrad);
			
			
			System.out.print("Unesi naziv :");
			String gradNaziv = PomocnaKlasa.ocitajTekst();
			grad.setNaziv(gradNaziv);

			
	
			GradDAO.update(AplicationUI.conn, grad);
		}
	}

	// brisanje studenta
	public static void brisanjePodatakaOGradu() {
		Grad grad = pronadjiGrad();
		if (grad != null) {
			GradDAO.delete(AplicationUI.conn, grad.getId());
		}
	}
	
}
