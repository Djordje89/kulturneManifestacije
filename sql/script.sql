use vezbe8;

drop table if exists grad;
CREATE TABLE grad (
  id_grad INT NOT NULL AUTO_INCREMENT,
  ptt VARCHAR(45) NOT NULL,
  naziv VARCHAR(45) NOT NULL,
  PRIMARY KEY (id_grad))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



drop table if exists manifestacija;
CREATE TABLE manifestacija (
  id_manifestacija INT NOT NULL AUTO_INCREMENT,
  naziv VARCHAR(45) NOT NULL,
  brojPosetilaca VARCHAR(45) NOT NULL,
  PRIMARY KEY (id_manifestacija))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;




drop table if exists odrzava;
create table odrzava (
	id_grad  integer not null,
	id_manifestacija  integer not null,
	PRIMARY KEY(id_grad, id_manifestacija),
	
	FOREIGN KEY (id_grad)
	                        REFERENCES grad(id_grad)
	                        ON DELETE RESTRICT,
	
	FOREIGN KEY (id_manifestacija)
	                        REFERENCES manifestacija(id_manifestacija)
	                        ON DELETE RESTRICT
	
	); 
    
INSERT INTO grad (id_grad, ptt, naziv) VALUES ('1', '11000', 'Beograd');
INSERT INTO grad (id_grad, ptt, naziv) VALUES ('2', '21000', 'Novi Sad');
INSERT INTO grad (id_grad, ptt, naziv) VALUES ('3', '34000', 'Kragujevac');


INSERT INTO manifestacija (id_manifestacija, naziv, brojPosetilaca) VALUES ('1', 'Sajam Knjiga', '34000');
INSERT INTO manifestacija (id_manifestacija, naziv, brojPosetilaca) VALUES ('2', 'Exit', '41500');
INSERT INTO manifestacija (id_manifestacija, naziv, brojPosetilaca) VALUES ('3', 'Sajam automobila', '23000');
INSERT INTO manifestacija (id_manifestacija, naziv, brojPosetilaca) VALUES ('4', 'Dani tamburice', '56000');
INSERT INTO manifestacija (id_manifestacija, naziv, brojPosetilaca) VALUES ('5', 'Dani Muzeja', '16000');


INSERT INTO odrzava (id_grad, id_manifestacija) VALUES ('1', '1');
INSERT INTO odrzava (id_grad, id_manifestacija) VALUES ('2', '2');
INSERT INTO odrzava (id_grad, id_manifestacija) VALUES ('2', '3');
INSERT INTO odrzava (id_grad, id_manifestacija) VALUES ('2', '5');
INSERT INTO odrzava (id_grad, id_manifestacija) VALUES ('3', '4');

    
